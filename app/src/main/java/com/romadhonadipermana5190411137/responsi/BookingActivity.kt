package com.romadhonadipermana5190411137.responsi

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class BookingActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_booking)
        val btnBackHome = findViewById<Button>(R.id.btn_back_home)

        btnBackHome.setOnClickListener {
            finish()
        }
    }
}