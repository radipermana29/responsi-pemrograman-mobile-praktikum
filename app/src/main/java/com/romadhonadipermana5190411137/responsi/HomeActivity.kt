package com.romadhonadipermana5190411137.responsi

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.LinearLayout

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val email = intent.getStringExtra("email")
        val btnBookingKereta = findViewById<LinearLayout>(R.id.btn_booking_kereta)
        val btnHistory = findViewById<LinearLayout>(R.id.btn_history)
        val btnBookingHotel = findViewById<LinearLayout>(R.id.btn_booking_hotel)
        val btnProfile = findViewById<LinearLayout>(R.id.btn_profile)
        val btnLogout = findViewById<Button>(R.id.btn_logout)

        btnLogout.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
        btnBookingKereta.setOnClickListener {
            startActivity(Intent(this, BookingActivity::class.java))
        }
        btnHistory.setOnClickListener {
            startActivity(Intent(this, HistoryActivity::class.java))
        }
        btnBookingHotel.setOnClickListener {
            startActivity(Intent(this, BookingHotelActivity::class.java))
        }
        btnProfile.setOnClickListener {
            val i = Intent(this, ProfileActivity::class.java)
            i.putExtra("email", email)
            startActivity(i)
        }
    }
}