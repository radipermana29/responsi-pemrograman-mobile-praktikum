package com.romadhonadipermana5190411137.responsi

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class BookingHotelActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_booking_hotel)

        val btnBackHome = findViewById<Button>(R.id.btn_back_home)

        btnBackHome.setOnClickListener {
            finish()
        }
    }
}