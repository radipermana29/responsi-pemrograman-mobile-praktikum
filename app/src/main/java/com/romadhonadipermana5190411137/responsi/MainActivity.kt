package com.romadhonadipermana5190411137.responsi

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val etEmail = findViewById<EditText>(R.id.et_email)
        val etPassword = findViewById<EditText>(R.id.et_password)
        val btnLogin = findViewById<Button>(R.id.btn_login)

        btnLogin.setOnClickListener {
            var email = etEmail.text.toString()
            var passwd = etPassword.text.toString()
            if (email.trim().isEmpty()){
                etEmail.error = "Required"
                Toast.makeText(applicationContext, "Masukkan Email!", Toast.LENGTH_SHORT).show()
            }else if (passwd.trim().isEmpty()){
                etPassword.error = "Required"
                Toast.makeText(applicationContext, "Masukkan password!", Toast.LENGTH_SHORT).show()
            } else {
                var i = Intent(this, HomeActivity::class.java)
                i.putExtra("email", email.trim())
                startActivity(i)
                finish()
            }
        }
    }
}