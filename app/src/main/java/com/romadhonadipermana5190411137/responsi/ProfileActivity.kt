package com.romadhonadipermana5190411137.responsi

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.ListView
import android.widget.TextView

class ProfileActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        val email = intent.getStringExtra("email")
        val btnBackHome = findViewById<Button>(R.id.btn_back_home)
        val txtNamaProfile = findViewById<TextView>(R.id.txt_nama_profile)

        val arrayAdapter: ArrayAdapter<*>
        val settings = arrayOf("Pengaturan Akun", "Pengaturan Notifikasi", "Pengaturan Privasi",
        "Pertanyaan dan Bantuan", "Tentang Aplikasi")

        txtNamaProfile.text = email

        btnBackHome.setOnClickListener {
            finish()
        }

        val settingList = findViewById<ListView>(R.id.list_profile)
        arrayAdapter = ArrayAdapter(this,
            android.R.layout.simple_expandable_list_item_1, settings)
        settingList.adapter = arrayAdapter
    }
}